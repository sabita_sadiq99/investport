import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sign_button/sign_button.dart';
import './forgot_password.dart';
import './sign_in.dart';
import './welcome.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _rememberMe = false;


  Widget _buildEmailTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
       
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
         
          height: 80.0,
          child: new TextField(
	autocorrect: true,
	decoration: InputDecoration(
    hintText: 'Email', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),),
        ),
      ],
    );
  }

  Widget _buildPasswordTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
       
        SizedBox(height: 0.0),
        Container(
          alignment: Alignment.centerLeft,
          
          height: 80.0,
          child: TextField(
	autocorrect: true,
   obscureText: true,
	decoration: InputDecoration(
    hintText: 'Password', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),),
        ),
      ],
    );
  }

  Widget _buildForgotPasswordBtn() {
    return Container(
      alignment: Alignment.centerRight,
      child: FlatButton(
         onPressed: () {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => ForgotPass()),
  );
},
        padding: EdgeInsets.only(right: 0.0),
        child: Text(
          'Forgot Password?',
          style: GoogleFonts.montserrat(  color: HexColor('#0071BC') ,fontWeight: FontWeight.w500,fontSize: 16.0,),
         
        ),
      ),
    );
  }

  Widget _buildRememberMeCheckbox() {
    return Container(
      
      height: 20.0,
      child: Row(
        children: <Widget>[
          Theme(
            data: ThemeData(unselectedWidgetColor: Colors.grey,),
            child: Checkbox(
              value: _rememberMe,
              checkColor: Colors.green,
              activeColor: Colors.white,
              onChanged: (value) {
                setState(() {
                  _rememberMe = value!;
                });
              },
            ),
          ),
         Text('Keep me logged in',style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
         )),
        ],
      ),
    );
  }

  Widget _buildLoginBtn() {
    return Container(
          width:350,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 90.0,left: 90.0,top: 15.0,bottom: 15.0),child: Text(
        "Sign In",
        style: GoogleFonts.montserrat(fontSize: 20,fontWeight: FontWeight.w600 )
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(HexColor("#00A14B")),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#00A14B"))
          )
        )
      ),
        onPressed: () {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Welcome()),
  );
}
    ),
                  );
  }

 

  

  

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      backgroundColor: Colors.white,
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
             
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 10.0,
                    vertical: 100.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Login',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                         
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height:10.0),
                      Text('Please login to continue',style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                          
                          fontSize: 18.0,
                          letterSpacing: 0.2 ,
                          fontWeight: FontWeight.w500,
                         
                        ),),
                      SizedBox(height: 30.0),
                      _buildEmailTF(),
                     
                      _buildPasswordTF(),
                       SizedBox(height: 20.0),
                      
                      _buildLoginBtn(),
                      Row(
                   
                        children:<Widget>[
                             
                      _buildRememberMeCheckbox(),
                        Spacer(),
                       _buildForgotPasswordBtn(),
                      
                        ]
                      ),
                    
                      // _buildSignInWithText(),
                      // _buildSocialBtnRow(),
             Row(children: <Widget>[
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 10.0, right: 15.0),
                  child: Divider(
                    color: Colors.black,
                    thickness: 1,
                    height:50,
                  )),
            ),
 
            Text("OR"),
 
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 15.0, right: 10.0),
                  child: Divider(
                    color: Colors.black,
                      thickness: 1,
                    height: 30,
                  )),
            ),
          ]),
           SizedBox(height:15.0),
                  
                       
                          SignInButton(
                          buttonType: ButtonType.google,
                          buttonSize: ButtonSize.large,
                    
                          onPressed: () {
                          signInWithGoogle().then((result) {
                              Navigator.of(context).push(
              MaterialPageRoute(
                builder: (
                  context,
                ) {
                  // _showLoadingDialog();
                  return ForgotPass(
                    
                  );
                },
              ),
            );
                          });
                          }),
                            SizedBox(height:15.0),
                          SignInButton(
                          buttonType: ButtonType.facebookDark,
                          buttonSize: ButtonSize.large, 
                          // small(default), medium, large
                          onPressed: () {
                          print('click');
                          }),
                           SizedBox(height:15.0),
                          SignInButton(
                          buttonType: ButtonType.linkedin,
                          buttonSize: ButtonSize.large, // small(default), medium, large
                          onPressed: () {
                          print('click');
                          }),
                          SizedBox(height:10.0),
                          Row(
                            children:[
                              Spacer(),
                                 Text("Don't have an Account?",style: TextStyle(fontSize:18.0),),
                          Text(" Sign Up",style: TextStyle(fontSize:18.0,color:HexColor('#0071BC') ),),
                          Spacer(),
                            ]
                          ),
                         
                        
                                            ],
                                          ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}