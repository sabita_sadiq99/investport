import 'dart:ui';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import './setting.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';

class Welcome extends StatefulWidget{
  _WelcomeState createState() => _WelcomeState();
}
class _WelcomeState extends State<Welcome>{
   final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();


  void _onItemTapped(int index) {
    if(index==0)
    {
       Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Welcome()),
  );
    }

   if(index == 4)
   {
      Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Setting()),
  );
   }
  }
    Widget build(BuildContext context) {
    
    return Scaffold(
      key: _scaffoldKey,
    drawer: Drawer(
        child: Container(
          //child: Your widget,
          color: HexColor('#1b2f35'),
          width: double.infinity,
          height: double.infinity,
          child: ListView(
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: HexColor('#1b2f35'),
              ),
              child: Stack(
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft+ Alignment(0.2,0),
                    child: new Image.asset(
              'logo-white.png',
              width: 100.0,
              height: 100.0,
              fit: BoxFit.contain,
            ),
            
                  ),
                 
                  Align(
                    alignment: Alignment.bottomLeft + Alignment(0.2,1.5),
                    child: CircleAvatar(
                      backgroundImage: NetworkImage('https://avatars0.githubusercontent.com/u/8264639?s=460&v=4'),
                      radius: 30.0,
                    ),
                  ),
                  Align(
                    alignment: Alignment(0.4, 1.5),
                    child: Text(
                      'Alec Reynolds',
                      style: TextStyle(color: Colors.white, fontSize: 20.0),
                    ),
                  ),
                  SizedBox(height: 2.0,),
                 Align(
                    alignment: Alignment(0.5, 1.8),
                    child: Text(
                      'johndoe@gmail.com',
                      style: TextStyle(color: Colors.white, fontSize: 15.0),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height:50.0),
             new Divider(
            color: Colors.white,
            thickness: 0.8,
          ),
          ListTile(
                leading: Icon(Icons.home,color: Colors.white,),
                title: Text('Home',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: null,
              ),
               ListTile(
                leading: Icon(Icons.shopping_bag,color: Colors.white,),
                title: Text('My Orders',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: null,
              ),
              ListTile(
                leading: Icon(Icons.account_balance_wallet,color: Colors.white,),
                title: Text('My Wallet',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: null,
              ),
              ListTile(
                leading: Icon(Icons.settings,color: Colors.white,),
                title: Text('Account Settings',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: () {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Setting()),
  );
},
              ),
               ListTile(
                leading: Icon(Icons.power_settings_new_rounded,color: Colors.white,),
                title: Text('Sign Out',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: null,
              ),
          ],
          
        ),
        ),
      ),
      backgroundColor: Colors.white,
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
             
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 0.0,
                    vertical: 60.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      
                      Row(
                        children:[
                    Spacer(),
                               CircularProfileAvatar(
          'https://avatars0.githubusercontent.com/u/8264639?s=460&v=4', //sets image path, it should be a URL string. default value is empty string, if path is empty it will display only initials
          radius: 20, // sets radius, default 50.0              
          backgroundColor: Colors.transparent, // sets background color, default Colors.white
          // borderWidth: 10,  // sets border, default 0.0

          // borderColor: Colors.brown, // sets border color, default Colors.white
          elevation: 5.0, // sets elevation (shadow of the profile picture), default value is 0.0
          // foregroundColor: Colors.brown.withOpacity(0.5), //sets foreground colour, it works if showInitialTextAbovePicture = true , default Colors.transparent
          cacheImage: true, // allow widget to cache image against provided url
          onTap: () => _scaffoldKey.currentState!.openDrawer(),
          showInitialTextAbovePicture: true, // setting it true will show initials text above profile picture, default false  
          ),
          Spacer(),
          
              Text(
                        'Welcome!',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                         
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Spacer(),
                      Spacer(),
                        ]
                      ),
                      SizedBox(height:10.0),
                      Text('Choose where you love to Invest in',style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                          
                          fontSize: 18.0,
                          letterSpacing: 0.1 ,
                          fontWeight: FontWeight.w500,
                         
                        ),),
                        SizedBox(
                          height:20.0,
                        ),
                        Container(
  height: 51,
  width: MediaQuery.of(context).size.width / 1.1,
  child: Row(
    children: [
      
      Expanded(
        child: new Theme(
          data: new ThemeData(
            primaryColor: HexColor('#0071BC'),
            primaryColorDark: HexColor('#0071BC'),
          ),
          child:  TextField(
          decoration: InputDecoration(
           hintText: 'Search Here',
      enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: HexColor('#0071BC'),),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: HexColor('#0071BC'),),
	  ),
            fillColor: Colors.white,
            filled: true,
            suffixIcon: Icon(Icons.search,color: HexColor('#0071BC')
            ),
          ),
        ),) 
       
      ),
      SizedBox(width: 10),
      Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(9.0),
        ),
        child: IconButton(
          icon: Icon(Icons.tune,color: HexColor('#0071BC'),size: 40.0,),
          onPressed: null,
        ),
      ),
    ],
  ),
),
 ],
  ),
                ),
              ),
             
            ],
          ),
        ),
      ),
      bottomNavigationBar:BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
            
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favourites',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_bag_outlined),
            label: 'My Orders',
          ),
            BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
            
          ),
       
          
        ],
        iconSize: 30.0, 
        selectedItemColor: HexColor('#0071BC'),
        unselectedItemColor: HexColor('#7C7D7E'),
        unselectedLabelStyle: TextStyle(color:Colors.black),
        onTap: _onItemTapped,
      ),
    );
  }
}