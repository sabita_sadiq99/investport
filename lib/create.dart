import 'package:flutter/material.dart';
import './login.dart';
import 'package:intl/intl.dart';
import 'dart:ui';
import 'package:flutter/gestures.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:validators/validators.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:sign_button/sign_button.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
class CreateAccount extends StatefulWidget{
  _CreateAccountState createState() => _CreateAccountState();
}
class _CreateAccountState extends State<CreateAccount>
{
   final _formKey = GlobalKey<FormState>();
     final _username = TextEditingController();
final _userPasswordController = TextEditingController();
final _email = TextEditingController();
   final _address = TextEditingController();
  final _cnic = TextEditingController();
  final _cvv =TextEditingController();
  
    String _myActivity= "";

  bool _passwordVisible = false;
  var items = ['item1', 'item2', 'item3', 'item4'];

  String formattedDate="";
  final TextEditingController controller = TextEditingController();
  String initialCountry = 'PK';
  PhoneNumber number = PhoneNumber(isoCode: 'PK');

  void initState() {
    _passwordVisible = false;
  }
  Widget build(BuildContext context) {
    
    return Scaffold(body: HomePageBody());
  }
  Widget HomePageBody(){
    return Container(
      height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal:10.0,
                    vertical:80.0,
                  ),
      
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
             Text(
                        'Sign Up',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                         
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                       SizedBox(height:12.0),
                      Text('Add your details to sign up',style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                          
                          fontSize: 18.0,
                          letterSpacing: 0.2 ,
                          fontWeight: FontWeight.w400,
                         
                        ),textAlign: TextAlign.center,),
          SizedBox(height : 16),
          NameInput(),
          SizedBox(height : 7),
          EmailInput(),
          SizedBox(height : 7),
          AddressInput(),
          SizedBox(height : 7),
          CNIC(),
          SizedBox(height : 7),
          PhoneInput(),
          SizedBox(height : 7),
          Row(
            children: [
              Flexible(child: ExpiryInput()),
              Flexible(child: CVVInput()),
            ],
          ),
          SizedBox(height : 7),
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //   children: [
          //   Flexible(child: CityInput()),
          //   Flexible(child: StateInput()),
          // ],),
           SizedBox(height : 7),
          
           SizedBox(height : 7),
           
           PasswordInput(),
           SizedBox(height : 7),
           ConfirmPassInput(),
            SizedBox(height : 10),
          _buildLoginBtn(),
           SizedBox(height:10.0),
                          Row(
                            children:[
                              Spacer(),
                              RichText(text: TextSpan(text: "Already have an Account?",style: TextStyle(fontSize:18.0, color: HexColor('#7C7D7E')),
                              children: <TextSpan>[
                                TextSpan(
                                  text:' Login',
                                   recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      Navigator.of(context).push(
              MaterialPageRoute(
                builder: (
                  context,
                ) {
                  // _showLoadingDialog();
                  return LoginScreen(
                    
                  );
                },
              ),
            );
                    },style: TextStyle(fontSize:18.0,color:HexColor('#0071BC')
                                )
                              )])),
                          Spacer(),
                            ]
                          ),
                               SizedBox(height:20.0),
                               Text("or sign up with", style: TextStyle(fontSize:18.0, color: HexColor('#7C7D7E'),fontWeight: FontWeight.bold) ,),
                               SizedBox(height:20.0),
                               Row(children: [
                                 Spacer(),
                                   SignInButton.mini(
 buttonType: ButtonType.google,
  buttonSize: ButtonSize.large,
 onPressed: () {},
),
  SignInButton.mini(
 buttonType: ButtonType.facebook,
  buttonSize: ButtonSize.large,
 onPressed: () {},
),
  SignInButton.mini(
 buttonType: ButtonType.linkedin,
 buttonSize: ButtonSize.large,
 onPressed: () {},
),
Spacer(),
                               ],)
                             

         
        ],),
      )
    ));
  }

  EdgeInsets edgeInsets() {
    return EdgeInsets.symmetric(
                  horizontal: 15.0,
                  vertical: 100.0,
                );
  }
  Widget PasswordInput()
  {
    return  TextFormField(
   keyboardType: TextInputType.text,
   controller: _userPasswordController,
   obscureText: !_passwordVisible,//This will obscure text dynamically
   decoration: InputDecoration(
    hintText: 'Password', contentPadding: const EdgeInsets.only(left:20.0,right:25.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
       // Here is key idea
       suffixIcon: IconButton(
            icon: Icon(
              // Based on passwordVisible state choose the icon
               _passwordVisible
               ? Icons.visibility
               : Icons.visibility_off,
               color: Theme.of(context).primaryColorDark,
               ),
            onPressed: () {
               // Update the state i.e. toogle the state of passwordVisible variable
               setState(() {
                   _passwordVisible = !_passwordVisible;
               });
             },
            ),
          ),
        );
  }
    Widget ConfirmPassInput()
  {
    return  TextFormField(
   keyboardType: TextInputType.text,
   controller: _userPasswordController,
   obscureText: !_passwordVisible,//This will obscure text dynamically
   decoration: InputDecoration(
    hintText: 'Confirm Password', contentPadding: const EdgeInsets.only(left:20.0,right:25.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
       // Here is key idea
       suffixIcon: IconButton(
            icon: Icon(
              // Based on passwordVisible state choose the icon
               _passwordVisible
               ? Icons.visibility
               : Icons.visibility_off,
               color: Theme.of(context).primaryColorDark,
               ),
            onPressed: () {
               // Update the state i.e. toogle the state of passwordVisible variable
               setState(() {
                   _passwordVisible = !_passwordVisible;
               });
             },
            ),
          ),
        );
  }
  //     Widget CountryInput() {
  //   return DropdownButtonFormField(
  //             decoration: InputDecoration(
  //   hintText: 'Country', contentPadding: const EdgeInsets.only(left:20.0,right:15.0,bottom: 35.0,top: 15.0),
	//   hintStyle: TextStyle(color: Colors.grey),
	//   filled: true,
	//   fillColor: Colors.grey[80],
	//   enabledBorder: OutlineInputBorder(
	// 	borderRadius: BorderRadius.all(Radius.circular(50.0)),
	// 	borderSide: BorderSide(color: Colors.white, width: 2),
	//    ),
	//   focusedBorder: OutlineInputBorder(
	// 	borderRadius: BorderRadius.all(Radius.circular(50.0)),
	// 	borderSide: BorderSide(color: Colors.white),
	//   ),
	// ),
  //             value: _myActivity,
  //             // ignore: non_constant_identifier_names
  //             onChanged: (String Value) {
  //               setState(() {
  //                 _myActivity = Value;
  //               });
  //             },
  //             items: items
  //                 .map((cityTitle) => DropdownMenuItem(
  //                     value: cityTitle, child: Text("$cityTitle")))
  //                 .toList(),
  //           );
        
  // }
  //      Widget StateInput() {
  //   return DropdownButtonFormField(
  //             decoration: InputDecoration(
  //   hintText: 'State', contentPadding: const EdgeInsets.only(left:20.0,right:15.0,bottom: 35.0,top: 15.0),
	//   hintStyle: TextStyle(color: Colors.grey),
	//   filled: true,
	//   fillColor: Colors.grey[80],
	//   enabledBorder: OutlineInputBorder(
	// 	borderRadius: BorderRadius.all(Radius.circular(50.0)),
	// 	borderSide: BorderSide(color: Colors.white, width: 2),
	//    ),
	//   focusedBorder: OutlineInputBorder(
	// 	borderRadius: BorderRadius.all(Radius.circular(50.0)),
	// 	borderSide: BorderSide(color: Colors.white),
	//   ),
	// ),
  //             value: _myActivity,
  //             onChanged: (String Value) {
  //               setState(() {
  //                 _myActivity = Value;
  //               });
  //             },
  //             items: items
  //                 .map((cityTitle) => DropdownMenuItem(
  //                     value: cityTitle, child: Text("$cityTitle")))
  //                 .toList(),
  //           );
        
  // }
  //    Widget CityInput() {
  //   return DropdownButtonFormField(
  //             decoration: InputDecoration(
  //   hintText: 'City', contentPadding: const EdgeInsets.only(left:30.0,right:15.0,bottom: 35.0,top: 15.0),
	//   hintStyle: TextStyle(color: Colors.grey),
	//   filled: true,
	//   fillColor: Colors.grey[80],
	//   enabledBorder: OutlineInputBorder(
	// 	borderRadius: BorderRadius.all(Radius.circular(50.0)),
	// 	borderSide: BorderSide(color: Colors.white, width: 2),
	//    ),
	//   focusedBorder: OutlineInputBorder(
	// 	borderRadius: BorderRadius.all(Radius.circular(50.0)),
	// 	borderSide: BorderSide(color: Colors.white),
	//   ),
	// ),
  //             value: _myActivity,
  //             onChanged: (String Value) {
  //               setState(() {
  //                 _myActivity = Value;
  //               });
  //             },
  //             items: items
  //                 .map((cityTitle) => DropdownMenuItem(
  //                     value: cityTitle, child: Text("$cityTitle")))
  //                 .toList(),
  //           );
        
  // }
   Widget NameInput() {
    return TextFormField(
      controller: _username,
    	decoration: InputDecoration(
    hintText: 'Name', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_username) {
        if (_username!.length < 3)
          return 'Name must be more than 2 charater';
        else if (_username == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(_username))
        {
          return 'Invalid Name';
        }
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
     Widget EmailInput() {
    return TextFormField(
      controller: _email,
    	decoration: InputDecoration(
    hintText: 'Email', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_email) {
        if (_email!.length < 3)
          return 'Name must be more than 2 charater';
        else if (_email == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(_email))
        {
          return 'Invalid Name';
        }
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
   Widget AddressInput() {
    return TextFormField(
      controller: _address,
    	decoration: InputDecoration(
    hintText: 'Address', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_address) {
        if (_address!.length < 3)
          return 'Name must be more than 2 charater';
        else if (_address == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(_address))
        {
          return 'Invalid Name';
        }
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
   Widget CNIC() {
    return TextFormField(
      controller: _cnic,
    	decoration: InputDecoration(
    hintText: 'CNIC', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_cnic) {
        if (_cnic!.length < 3)
          return 'Name must be more than 2 charater';
        else if (_cnic == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(_cnic))
        {
          return 'Invalid Name';
        }
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
  Widget ExpiryInput(){
    return TextFormField(
                        	decoration: InputDecoration(
    hintText: 'Expiry Date', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      onTap: (){
        
        // Below line stops keyboard from appearing
        FocusScope.of(context).requestFocus(new FocusNode());

          DatePicker.showDatePicker(context,
                          showTitleActions: true,
                          minTime: DateTime(2018, 3, 5),
                          maxTime: DateTime.now(), onChanged: (date) {
                        print('change $date');
                      }, onConfirm: (date) {
                        print('confirm $date');
                        setState(() {
                          var _date = date;
                          formattedDate =
                              DateFormat('dd-MM-yyyy').format(_date);
                        });
                      }, currentTime: DateTime.now(), locale: LocaleType.en);

      },
    );
  }
   Widget CVVInput() {
    return TextFormField(
      controller: _cvv,
    	decoration: InputDecoration(
    hintText: 'CVV', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_cvv) {
        if (_cvv!.length < 3)
          return 'Name must be more than 2 charater';
        else if (_cvv == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(_cvv))
        {
          return 'Invalid Name';
        }
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
    Widget PostalInput() {
    return TextFormField(
      controller: _username,
    	decoration: InputDecoration(
    hintText: 'Postal Code', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_username) {
        if (_username!.length < 3)
          return 'Name must be more than 2 charater';
        else if (_username == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(_username))
        {
          return 'Invalid Name';
        }
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
  Widget PhoneInput()
  {
      return InternationalPhoneNumberInput(
              onInputChanged: (PhoneNumber number) {
                //print(number.phoneNumber);
              },
              onInputValidated: (bool value) {
                if(value == true)
                {
                   print(value);
                   
                }
               
              },
              selectorConfig: SelectorConfig(
                selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
              ),
              inputDecoration: InputDecoration(
    hintText: 'Phone', contentPadding: const EdgeInsets.only(left:30.0,right:15.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
              ignoreBlank: false,
              autoValidateMode: AutovalidateMode.disabled,
              selectorTextStyle: TextStyle(color: Colors.black),
              initialValue: number,
              textFieldController: controller,
              formatInput: false,
              keyboardType:
                  TextInputType.numberWithOptions(signed: true, decimal: true),
              inputBorder: OutlineInputBorder(),
              onSaved: (PhoneNumber number) {
                print('On Saved: $number');
              },
            );
  }
    
  Widget _buildLoginBtn() {
    return Container(
          width:400,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 100.0,left: 90.0,top: 15.0,bottom: 15.0),child: Text(
        "Sign Up",
        style: GoogleFonts.montserrat(fontSize: 20,fontWeight: FontWeight.w600 )
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(HexColor("#00A14B")),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#00A14B"))
          )
        )
      ),
      onPressed: null,
//         onPressed: () {
//   Navigator.push(
//     context,
//     MaterialPageRoute(builder: (context) => LoginScreen()),
//   );
// }
    ),
                  );
  }
    
}