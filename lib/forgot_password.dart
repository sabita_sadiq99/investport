import 'dart:ui';

import 'package:flutter/material.dart';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import './pin_input.dart';

class ForgotPass extends StatefulWidget{
  _ForgotPassState createState() => _ForgotPassState();
}
class _ForgotPassState extends State<ForgotPass>{
   Widget _buildEmailTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
       
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
         
          height: 80.0,
          child: new TextField(
	autocorrect: true,
	decoration: InputDecoration(
    hintText: 'Email', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),),
        ),
      ],
    );
  }
  Widget _buildLoginBtn() {
    return Container(
          width:350,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 90.0,left: 90.0,top: 15.0,bottom: 15.0),child: Text(
        "Send",
        style: GoogleFonts.montserrat(fontSize: 20,fontWeight: FontWeight.w600 )
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(HexColor("#00A14B")),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#00A14B"))
          )
        )
      ),
      
        onPressed: () {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => PinPutApp()),
  );
}
    ),
                  );
  }

  Widget build(BuildContext context)
  {
   return Scaffold(
      backgroundColor: Colors.white,
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
             
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 25.0,
                    vertical: 100.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Forgot Password',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                         
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height:10.0),
                      Text('Please enter your email to receive a link to create a new password via email',style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                          
                          fontSize: 18.0,
                          letterSpacing: 0.2 ,
                          fontWeight: FontWeight.w500,
                         
                        ),
                        textAlign: TextAlign.center,),
                      SizedBox(height: 30.0),
                      _buildEmailTF(),
                     
                      
                       SizedBox(height: 20.0),
                      
                      _buildLoginBtn(),
                      
                    
                      // _buildSignInWithText(),
                      // _buildSocialBtnRow(),
          
           
                         
                        
                                            ],
                                          ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}