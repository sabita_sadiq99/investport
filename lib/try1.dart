import 'package:flutter/material.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart'; 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
 
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
           appBar: AppBar(
             title: Text('Horizontal Progress Bar')
             ),
            body: Center(
              child: HorizontalProgressIndicator()
              )
            )
          );
  }
}
 
class HorizontalProgressIndicator extends StatefulWidget {
  @override
  HorizontalProgressIndicatorState createState() => new HorizontalProgressIndicatorState();
}
 
class HorizontalProgressIndicatorState extends State<HorizontalProgressIndicator>
    with SingleTickerProviderStateMixin {
      
  late AnimationController controller;
 
  late Animation animation;
 
  double beginAnim = 0.0 ;
  double endAnim = 1.0 ;
 
  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(seconds: 5), vsync: this);
        animation = Tween(begin: beginAnim, end: endAnim).animate(controller)
          ..addListener(() {
            setState(() {
              // Change here any Animation object value.
            });
          });
    }
 
  @override
  void dispose() {
    controller.stop();
    super.dispose();
  }
 
  startProgress(){
 
    controller.forward();
  }
 
  stopProgress(){
 
    controller.stop();
  }
 
  resetProgress(){
 
    controller.reset();
  }
 
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(children: [
          Container(
          padding: EdgeInsets.all(20.0),
          child: LinearProgressIndicator(
            value: animation.value,
          )),
 
          RaisedButton(
            child: Text(" Start Progress "),
            onPressed: startProgress,
            color: Colors.red,
            textColor: Colors.white,
            splashColor: Colors.grey,
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
          ),
 
          RaisedButton(
            child: Text(" Stop Progress "),
            onPressed: stopProgress,
            color: Colors.red,
            textColor: Colors.white,
            splashColor: Colors.grey,
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
          ),
 
          RaisedButton(
            child: Text(" Reset Progress "),
            onPressed: resetProgress,
            color: Colors.red,
            textColor: Colors.white,
            splashColor: Colors.grey,
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
          )
        ]
    ));
  }
}