import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import './account_menu.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({ Key? key, required this.title}) : super(key: key);


  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
 

 

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
       
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
           new Image.asset(
              'logo-white.png',
              width: 200.0,
              height: 200.0,
              fit: BoxFit.contain,
            ),
            SizedBox(
              height: 10.0
            ),
            ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 90.0,left: 90.0,top: 15.0,bottom: 15.0),child: Text(
        "Get Started",
        style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold ,fontFamily: 'Montserrat')
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(HexColor("#00A14B")),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#00A14B"))
          )
        )
      ),
    onPressed: () {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Account_select()),
  );
}
    )
          ],
        ),
      ),
  
    );
  }
}
