import 'dart:ui';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/gestures.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart'; 
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:validators/validators.dart';
import './welcome.dart';
class Setting extends StatefulWidget{
  _SettingState createState() => _SettingState();
}
class _SettingState extends State<Setting>{
  final _formKey = GlobalKey<FormState>();
   final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
 int _selectedIndex = 4;
 int currentstate =0 ;
 int formstate=0;
final _username = TextEditingController();
final _email = TextEditingController();
final _phone = TextEditingController();
final _address = TextEditingController();
final _address2 = TextEditingController();
final _cnic = TextEditingController();
final city = TextEditingController();
final country = TextEditingController();
    void _onItemTapped(int index) {
    if(index==0)
    {
       Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Welcome()),
  );
    }

   if(index == 4)
   {
      Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Setting()),
  );
   }
  }
    Widget build(BuildContext context) {
    
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
             
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 20.0,
                    vertical: 60.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      
                      Row(
                        children:[
Spacer(),

                             IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: HexColor('#0071BC'),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
          Spacer(),
          
              Text(
                        'Account Setting',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                         
                          fontSize: 22.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Spacer(),
                    Spacer(),
                    Spacer(),
                        ]
                      ),
                      SizedBox(height:10.0),
                      ImageProfile(),
                        SizedBox(height:30.0),
                        Padding(padding: EdgeInsets.only(left:10.0,right:10.0),
                        child:StepProgressIndicator(
    totalSteps: 5,
    currentStep: currentstate,
    size: 12,
    padding: 5.0,
    unselectedColor: Colors.grey[350]!,
    selectedColor: Colors.green,
)

                        ),
                        Form(
                          key:_formKey,
                          child:Column(children: [
                             SizedBox(height:20.0),
                             NameInput(),
                             SizedBox(height:10.0),
                             EmailInput(),
                              SizedBox(height:10.0),
                            Phone(),
                            SizedBox(height:10.0),
                            Address(),
                            SizedBox(height:10.0),
                            Address2(),
                            SizedBox(height:10.0),
                            CNIC(),
                            SizedBox(height:10.0),
                            City(),
                            SizedBox(height:10.0),
                            Country(),
                            SizedBox(height:10.0),
                            Align(
                              alignment: Alignment.bottomLeft+ Alignment(0.1,0)
                              ,child:
                               RichText(text: TextSpan(text:'Change Password',recognizer: TapGestureRecognizer()..onTap= (){

                            },style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                          fontSize: 15.0,
                          decoration: TextDecoration.underline,
                        ), ))
                            ),
                                    SizedBox(height:10.0),
                            Align(
                              alignment: Alignment.bottomLeft+ Alignment(0.1,0)
                              ,child:
                               RichText(text: TextSpan(text:'Upload CNIC Image',recognizer: TapGestureRecognizer()..onTap= (){

                            },style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                          fontSize: 15.0,
                         
                        ), ))
                            )
                           
                          ],)
                        ),
                        RaisedButton(onPressed: () {
                          print(formstate);
                           if (_formKey.currentState!.validate()) {
                          _formKey.currentState!.save();
                          if(formstate %2 ==0)
                           {
                         setState(() {
                             currentstate = currentstate +1;
                             formstate =0;
                      });}
                         }},)],),
                ),
              ),
             
            ],
          ),
        ),
      ),
      bottomNavigationBar:BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favourites',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_bag_outlined),
            label: 'My Orders',
          ),
            BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
            
          ),
       
          
        ],
        iconSize: 30.0,
        currentIndex: _selectedIndex,
        selectedItemColor: HexColor('#0071BC'),
        unselectedItemColor: HexColor('#7C7D7E'),
        unselectedLabelStyle: TextStyle(color:Colors.black),
        onTap: _onItemTapped,
      ),
    );
  }
  Widget ImageProfile()
  {
    return Center(
      child:Stack(children: [
        CircleAvatar(radius:60.0,backgroundImage: NetworkImage('https://avatars0.githubusercontent.com/u/8264639?s=460&v=4'),),
        Positioned(
          bottom: 10.0,
          left: 95.0,
          right: 0.0,
        child: InkWell(
         
          onTap: (){},
          child: Icon(Icons.camera_alt,
          color: Colors.black,size: 28.0,),
        ))
      ],)
    );
  }
     Widget NameInput() {
    return TextFormField(
      controller: _username,
    	decoration: InputDecoration(
    hintText: 'Name', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0), 
	  hintStyle: TextStyle(color: Colors.black),
	  filled: true,
	  fillColor: Colors.grey[80],
    errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_username) {
        if (_username!.length < 3)
          return 'Name must be more than 2 charater';
        else if (_username == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(_username))
        {
          return 'Invalid Name';
        }
        else
        {
          setState(() {
            formstate = formstate +1;
          });
          return null;
          
        }
          
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
     Widget EmailInput() {
    return TextFormField(
      controller: _email,
    	decoration: InputDecoration(
    hintText: 'Email', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.black),
	  filled: true,
	  fillColor: Colors.grey[80],
      errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_email) {
        if (_email!.length < 3)
          return 'Name must be more than 2 charater';
        else if (_email == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(_email))
        {
          return 'Invalid Name';
        }
        else
         setState(() {
            formstate = formstate +1;
          });
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
   Widget Phone() {
    return TextFormField(
      controller: _phone,
    	decoration: InputDecoration(
    hintText: 'Cell Number', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.black),
	  filled: true,
	  fillColor: Colors.grey[80],
      errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_phone) {
        if (_phone!.length < 3)
          return 'Name must be more than 2 charater';
        else if (_phone == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(_phone))
        {
          return 'Invalid Name';
        }
        else
         setState(() {
            formstate = formstate +1;
          });
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
    Widget Address() {
    return TextFormField(
      controller: _address,
    	decoration: InputDecoration(
    hintText: 'Address 1', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.black),
	  filled: true,
	  fillColor: Colors.grey[80],
      errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_address) {
        if (_address!.length < 3)
          return 'Name must be more than 2 charater';
        else if (_address == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(_address))
        {
          return 'Invalid Name';
        }
        else
         setState(() {
            formstate = formstate +1;
          });
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
     Widget Address2() {
    return TextFormField(
      controller: _address2,
    	decoration: InputDecoration(
    hintText: 'Address 2', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.black),
	  filled: true,
	  fillColor: Colors.grey[80],
      errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
    
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
    Widget CNIC() {
    return TextFormField(
      controller: _cnic,
    	decoration: InputDecoration(
    hintText: 'CNIC Number', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.black),
	  filled: true,
	  fillColor: Colors.grey[80],
      errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_cnic) {
        if (_cnic!.length < 3)
          return 'Name must be more than 2 charater';
        else if (_cnic == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(_cnic))
        {
          return 'Invalid Name';
        }
        else
         setState(() {
            formstate = formstate +1;
          });
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
  Widget City() {
    return TextFormField(
      controller: _cnic,
    	decoration: InputDecoration(
    hintText: 'City', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.black),
	  filled: true,
	  fillColor: Colors.grey[80],
      errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_cnic) {
        if (_cnic!.length < 3)
          return 'Name must be more than 2 charater';
        else if (_cnic == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(_cnic))
        {
          return 'Invalid Name';
        }
        else
         setState(() {
            formstate = formstate +1;
          });
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
  Widget Country() {
    return TextFormField(
      controller: _cnic,
    	decoration: InputDecoration(
    hintText: 'Country', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.black),
	  filled: true,
	  fillColor: Colors.grey[80],
      errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_cnic) {
        if (_cnic!.length < 3)
          return 'Name must be more than 2 charater';
        else if (_cnic == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(_cnic))
        {
          return 'Invalid Name';
        }
        else
         setState(() {
            formstate = formstate +1;
          });
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
}